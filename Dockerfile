FROM bitnami/kubectl:latest

ENV TZ America/Mexico_City
ENV LANG es_MX.UTF-8
ENV LANGUAGE es_MX.UTF-8
ENV LC_ALL es_MX.UTF-8

USER root

RUN apt-get update \
	&& apt-get install -y --no-install-recommends tzdata locales tar curl gettext-base \
	&& locale-gen ${LC_ALL} && update-locale

USER 1001