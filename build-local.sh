#! /bin/sh 

export set DOCKER_FILE=Dockerfile
export set DOCKER_REGISTRY=local
export set GROUPID=startup
export set COMPONENT=kubectl
export set VERSION=1.0.0.RELEASE
export set DOCKER_TAG=$DOCKER_REGISTRY/$GROUPID/$COMPONENT:$VERSION

echo -----------------------------------------------------------------
echo Builing Enviroment.... 

echo DOCKER_FILE = $DOCKER_FILE
echo GROUPID = $GROUPID
echo COMPONENT = $COMPONENT
echo VERSION = $VERSION
echo DOCKER_TAG = $DOCKER_TAG
echo -----------------------------------------------------------------

docker build -f $DOCKER_FILE -t $DOCKER_TAG .

